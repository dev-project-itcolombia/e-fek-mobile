import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CodeConfirmationPage } from '../code-confirmation/code-confirmation';

@IonicPage()
@Component({
  selector: 'page-user-checkout',
  templateUrl: 'user-checkout.html',
})
export class UserCheckoutPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserCheckoutPage');
  }


  public goToConfirmation() {
    this.navCtrl.push(CodeConfirmationPage);
  }
}
