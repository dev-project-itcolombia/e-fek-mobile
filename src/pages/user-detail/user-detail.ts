import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserCheckoutPage } from '../user-checkout/user-checkout';

@IonicPage()
@Component({
  selector: 'page-user-detail',
  templateUrl: 'user-detail.html',
})
export class UserDetailPage {

  public currentDate: string = new Date().toLocaleDateString();

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserDetailPage');
  }

  public goToCheckout() {
    this.navCtrl.push(UserCheckoutPage);
  }

}
