import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { IonicPage } from 'ionic-angular';
import { CancelPage } from '../cancel/cancel';
import { MovementPage } from '../movement/movement';
import { NewsPage } from '../news/news';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = MovementPage;
  tab3Root = CancelPage;
  tab4Root = NewsPage;

  constructor() {
  }


}
