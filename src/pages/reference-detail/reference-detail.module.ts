import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReferenceDetailPage } from './reference-detail';

@NgModule({
  declarations: [
    ReferenceDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(ReferenceDetailPage),
  ],
})
export class ReferenceDetailPageModule {}
