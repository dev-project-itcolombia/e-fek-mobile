import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ReferenceDetailPage } from '../reference-detail/reference-detail';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public currentDate: string = new Date().toLocaleDateString();

  constructor(public navCtrl: NavController) {

  }

  public goToDetail() {
    this.navCtrl.push(ReferenceDetailPage);
  }

}
