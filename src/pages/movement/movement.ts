import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserDetailPage } from '../user-detail/user-detail';

@IonicPage()
@Component({
  selector: 'page-movement',
  templateUrl: 'movement.html',
})
export class MovementPage {

  public currentDate: string = new Date().toLocaleDateString();

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MovementPage');
  }

  public findUser() {
    this.navCtrl.push(UserDetailPage);
  }

}
