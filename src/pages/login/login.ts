import { TabsPage } from './../tabs/tabs';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public loginWithCredentials: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public qrScanner: BarcodeScanner
  ) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  public changeLoginMode() {
    this.loginWithCredentials = true;
  }

  public asociateDevice() {
    this.navCtrl.setRoot(TabsPage);
  }

  public scan() {
    let options = {
      prompt: "Escanea tu código QR"
    }
    this.qrScanner.scan(options).then((data) => {
      console.log(data);
    }).catch((error) => {
      console.log(error);
    })
  }

}
