import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-reference-detail',
  templateUrl: 'reference-detail.html',
})
export class ReferenceDetailPage {

  public currentDate: string = new Date().toLocaleDateString();

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReferenceDetailPage');
  }

}
