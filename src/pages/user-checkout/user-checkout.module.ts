import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserCheckoutPage } from './user-checkout';

@NgModule({
  declarations: [
    UserCheckoutPage,
  ],
  imports: [
    IonicPageModule.forChild(UserCheckoutPage),
  ],
})
export class UserCheckoutPageModule {}
