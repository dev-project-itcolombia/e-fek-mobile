import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { MovementPage } from '../pages/movement/movement';
import { CancelPage } from '../pages/cancel/cancel';
import { NewsPage } from '../pages/news/news';
import { ReferenceDetailPage } from '../pages/reference-detail/reference-detail';
import { UserDetailPage } from '../pages/user-detail/user-detail';
import { UserCheckoutPage } from '../pages/user-checkout/user-checkout';

import { BarcodeScanner } from "@ionic-native/barcode-scanner";
import { CodeConfirmationPage } from '../pages/code-confirmation/code-confirmation';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    LoginPage,
    MovementPage,
    CancelPage,
    NewsPage,
    ReferenceDetailPage,
    UserDetailPage,
    UserCheckoutPage,
    CodeConfirmationPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    LoginPage,
    MovementPage,
    CancelPage,
    NewsPage,
    ReferenceDetailPage,
    UserDetailPage,
    UserCheckoutPage,
    CodeConfirmationPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    BarcodeScanner,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
